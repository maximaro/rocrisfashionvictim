import { expect } from 'chai';

class Page {

    openHomePage(path = ''){
        browser.url('/' + path);
    }

    goTo(pageName){
        let pageBtn = $(`a=${pageName}`);
        pageBtn.waitForExist();
        pageBtn.click();
    }

    verifyImage(results) {
        results.forEach((result, index) => expect(result.isExactSameImage, 'the image ' + index + ' is not the same').to.be.true);
    }

    getPageScreenshot() {
        let image = browser.checkDocument({      
        });
        this.verifyImage(image);
    }

    scrollToElement(element){
        browser.execute((element2) => {
            document.querySelector(element2).scrollIntoView();
        }, element);
    }

    removeDynamicElements(dynamicElements){
        dynamicElements.value.forEach((elem) => {
            browser.execute((element) => {
                element.remove();
            }, elem);
        });
    }

    removeDynamicElement(dynamicElement){
    browser.execute((element) => {
        element.value.remove();
    }, dynamicElement);
}

}

export default Page;