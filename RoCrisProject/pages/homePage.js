import Page from './page';

var rp = require('request-promise');

//This is used for the API post function
const options = {
    method: 'POST',
    uri: 'https://jsonplaceholder.typicode.com/posts',
    body: {
        title: 'foo',
        body: 'bar',
        userId: 1
    },
    resolveWithFullResponse: true,
    json: true
};

class HomePage extends Page {

    get searchInputField() { return browser.element('#search_query_atm_menu_12'); }
    get slidePictures() { return browser.elements('.columns-container'); }
    get topMenuBar() { return bowser.element('#adtm_menu_inner'); }
    get dynamicSocialBlockElements() { return browser.elements('#social_block ul li'); }
    get fashionVictimLogo() { return browser.element('#header_logo'); }
    get wdioTopMenu() { return browser.element('.fixedHeaderContainer'); }
    get homePagecliwindow() { return browser.element('.cliwindow'); }
    get devGuideTwitterLink() { return browser.element('.twitterLink'); }

    //search for an object on fashion victim web app
    searchForObject(searchObject) {
        this.searchInputField.addValue(searchObject);
    }

    //GET API request on a dummy API from the web using 'request-promise' module
    myFirstGetApiRequest() {
        return rp.get({
            url: 'https://jsonplaceholder.typicode.com/todos/1',
            headers: {
                'Content-Type': 'application/json'
            },
            resolveWithFullResponse: true
        }).then(function (resp) {
            return (resp);
        });
    }

    //POST API request on a dummy API from the web using 'request-promise' module
    myFirstPostApiRequest() {
        return rp.post(options)
            .then(function (response){
               var body = JSON.parse(JSON.stringify(response));
                return body;
            })
            .error(function (err){
                console.log("The err is =========", `${err}`);
            });

        //other way of a POST method
        // return rp.post({
        //     url: 'https://jsonplaceholder.typicode.com/posts',
        //     headers: {
        //         'Content-type': 'application/json',
        //     },
        //     method: 'POST',
        //     body: JSON.stringify({
        //         title: "foo",
        //         body: "bar",
        //         userId: 1
        //     }),
        //     resolveWithFullResponse: true
        // }).then(function (resp) {
        //     var apiResponse = JSON.parse(JSON.stringify(resp));
        //     return (apiResponse);
        // }).catch(function (err) {
        //     console.log("The err is =========", `${err}`);
        // });
    };
}

export default new HomePage();