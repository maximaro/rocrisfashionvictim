import { expect } from 'chai';
import HomePage from '../pages/homePage';
import DBHelper from '../dbHelper';
import Queries from '../queries';

describe('fashionVictim home page', () => {
    const wdioMenuBar = ['Home', 'Developer Guide', 'API', 'Contribute'];
    const menuBarPages = ['BARBATI', 'FEMEI'];
    before(function () {
        // Queries.executeQuery();
        HomePage.openHomePage();
    });

    /*
    checks the title of the 'webdriverio' or the 'FashionVictim' page title.
    Note: To check one or another page change the baseUrl into the wdio.conf.js file
    */
    // it('Check home page title', () => {
    //     const title = browser.getTitle();
    // expect(title).to.equal('WebdriverIO - WebDriver bindings for Node.js');
    //     // expect(title).to.equal('FashionVictim | Haine Originale | Branduri de lux - Fashion Victim');
    // });

    //This is an API 'GET' request example test
    // it('my first GET API test', () => {
    //     let apiResponse = browser.call(function(){
    //         return HomePage.myFirstGetApiRequest();
    //     });
    //     expect(apiResponse.statusCode, 'The status code is different').to.be.equal(200);
    // });

    //This is an API 'POST' request example test
    // it('my first POST API request', () => {
    //     let apiResponse = browser.call(function () {
    //         return HomePage.myFirstPostApiRequest();
    //     })
    //     expect(apiResponse.statusCode, 'The status code is different').to.be.equal(201);
    // });

    // it('visual-regression for a single element', () => {
    //     HomePage.getPageScreenshot();
    // });

    //Removes some elements from a page and afterward triggers a screenshot
    // it('visual-regression for a full page with hide/remove element', () => {
    //     HomePage.removeDynamicElements(HomePage.slidePictures);
    //     HomePage.removeDynamicElement(HomePage.fashionVictimLogo);
    //     HomePage.getPageScreenshot();
    // });

    /*
    For each page from the specified array is triggering a screenshot
    Note: The test was done on 'FashionVictim'
    */
    //     menuBarPages.forEach(element => {
    //     it('screenshoot every page =>' + element, function(){
    //         $('a*=' + element).click();
    //         HomePage.getPageScreenshot();
    //     });
    // });

     /*
    For each page from the specified array is triggering a screenshot
    Note: The test was done on 'Webdriverio'
    */
    //     wdioMenuBar.forEach(element => {
    //         it('test' + element, () => {
    //             $(`a*=`+ element).click();
    //             if(HomePage.homePagecliwindow.isVisible()){
    //             HomePage.getPageScreenshot();
    //         }
    //     });
    // });

    /*
    Search for an object
    Note: The test was done on 'FashionVictim'
    */
    // it('search for an object', () => {
    //     const search = 'rochii';
    //     HomePage.searchForObject(search);
    // });

    /*
    Navigates to a specific page
    Note: The test was done on 'FashionVictim'
    */
    // it ('open FEMEI page', () =>  {
    //     HomePage.goTo('FEMEI');
    // });
});