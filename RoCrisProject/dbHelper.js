var encryptor = require('simple-encryptor')(browser.options.encryptionKey); // required for the encrypted password

const sql = require('mssql');
const config = {
    user : encryptor.decrypt(browser.options.dbUser), // the user must exist in your configuration file
    password : encryptor.decrypt(browser.options.dbHashedPassword), // the password must exist in your configuration file
    server : 'DESKTOP-MEIIFAN\\SQLEXPRESS', // the server where the database is hosted
    database : 'testingDB' // the database name
};

class DBHelper {
    
    execute(myquery) {
        return sql.connect(config).then(pool => {
            return pool.request().query(`${myquery}`);
        }).then(result => {
            return result;
        }).catch(err => {
            console.error('The following error has occured: ' + `${err}`);
        });
    }
    
    close() {
        sql.close();
    }
};

export default new DBHelper();
